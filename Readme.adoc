= Mass Assignment

//tag::abstract[]

Mass Assignment happens when additional
parameters are accepted by the program which
can update the data model fields.
Mass Assignment can result in overwriting an existing sensitive field,
or adding a new property to the object.

//end::abstract[]

This vulnerability is commonly identified in programming
language frameworks where a data model can be populated
via HTTP requests and sensitive fields are not ignored.

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.

=== Task 0

*Fork* and clone this repository.
Install `docker` and `make` on your system.

. Build the program: `make build`.
. Run it: `make run`.
. Run unit tests: `make test`. 
. Run security tests: `make securitytest`.

Note: The last test will fail. 

=== Task 1

Review the program code try to find out 
why security tests fails. 
Submit a sample request: 
`curl -X POST http://localhost:8080 -H 'Content-Type: application/json' -d '{"name": "foo", "email":"foo@bar.com" }'`

Note: Avoid looking at tests or patch file and try to
spot the vulnerable pattern on your own.

=== Task 2

The program is vulnerable to Mass Assignment.
Find how to patch this vulnerability.

=== Task 3

Review `test/appSecurity.test.js` and see how security tests
works. Review your patch from the previous task.
Make sure this time the security tests pass.
If you stuck, move to the next task.

=== Task 4

Check out the `patch` branch:

* Read link:cheatsheet.adoc[] and apply to your solution.
* Review the patched program.
* Run all tests.

=== Task 5

Push you code and make sure build is passed
Finally, send a pull request (PR).

//end::lab[]

//tag::references[]

== References

* OWASP Application Security Verification Standard 4.0, 5.1.2
* CWE 235
* https://github.com/OWASP/CheatSheetSeries/blob/a5ab44faca75dbbb2517583c337d010579d7ce81/cheatsheets/Mass_Assignment_Cheat_Sheet.md[Mass_Assignment_Cheat_Sheet.md]
* http://www.codeblocq.com/2015/12/Read-only-properties-in-JavaScript/[Read-Only properties in JavaScript]

//end::references[]

include::CONTRIBUTING.adoc[]

== License

See link:LICENSE[]
